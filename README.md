## About Food2Fork API

Food2Fork offers an API which exposes its powerful recipe discovery functions for your app to use. 
The API gives you access to our ever expanding recipe database, powerful ingredient search function, and social-media based ranking algorithm. 
---

Create a proxy for the below API (Search and Get) and deploy it to Cloudhub.

# Working Proxy for Food2Fork API

+ [License Agreement](#licenseagreement)
+ [Use Case](#usecase)
+ [Considerations](#considerations)
	* [APIs security considerations] With  API authentication using client id enforcement policy which inbuilt policy in API manager
+ [Run it!](#runit)as you wish
	* [Running on premise](#runonopremise)
	* [Running on Studio](#runonstudio)  
	Version: 6.2.1
    Build Id: 201612221722
	* [Running on Mule ESB stand alone](#runonmuleesbstandalone)
	* [Running on CloudHub](#runoncloudhub)
	* [Deployed to cloudhub with Application name foodingredient

	* [Properties is configered with diffrent environment like dev,uat and prod, while testing application please pass below key.
	
	env=dev  (for CloudHub)
	
	-Denv=dev for local studio
	
	-M-Denv=dev for on-prem stand alone server
	
	# Use Case <a name="usecase"/>
	
	1) API authentication using client id enforcement
	
	2) The search flow should remove any 'egg' recipes in the API response
	3) If a user searches for 'egg' recipes throw an error stating "This feature is not supported!" with the proper HTTP response and code
	4) If the get recipe returns a recipe which contains 'egg' as an ingredient, throw error "Recipe contains egg!"
	5) Implement proper error handling in case of backend API is down.
	6) A scalable solution so it can handle 100 simultaneous API calls when deployed to Cloudhub with 0.2 vCore and 1 workers, without data loss.
	7) Return the response as 'text/json'
	8) Use property files and secure sensitive properties such as passwords
	9) Secure the encryption key so it is not visible on the Cloudhub Admin console
	
	You can look at the RAML file to see the expected structure of the message to be sent to invoke the food2fork API.
	You can Mock it .Please open below link.
	#Credential for openning Exchange
	UserId=Manishdemo
	Password=Veritas@1994
	
	https://anypoint.mulesoft.com/exchange/b9613e35-6b17-4db1-85fc-6060344aa104/food2forkapi/api/1.0.0/
	
	
	Please Note: This is sample RAML for food2fork API which will be used for creating API Proxy using API Manager.
Information for Consumer:
Please register your application to https://www.food2fork.com/about/api to get API Key because its a mandatory Parameter
For Details about web based API and How it works .Please see the below link.


#CloudHub URL:
#Credential
	UserId=Manishdemo
	Password=Veritas@1994

https://anypoint.mulesoft.com/cloudhub/#/console/applications/cloudhub/foodingredient/dashboard

api_key for consuming Application=a527e7713cb08ba1b61f91c26941c42a


client_id=48178b80021e4a728ab5952705b5d33a&client_secret=14F69b01ec644D9Db19cfa0da0eb3a45


CloudHub URL:

http://foodingredient.us-e2.cloudhub.io/api/get?key=a527e7713cb08ba1b61f91c26941c42a&rId=6868&client_id=48178b80021e4a728ab5952705b5d33a&client_secret=14F69b01ec644D9Db19cfa0da0eb3a45





	
	
